﻿namespace astar_csharp
{
    public class Node
    {
        public Point Position;
        public Node Parent;
        public int G;
        public int H;
        public bool IsWalkable;
        public int F { get { return G + H; } }

        public Node(Point pos, bool walkable, Node parent = null)
        {
            Position = pos;
            Parent = parent;
            G = 0;
            H = 0;
            IsWalkable = walkable;
        }
    }
}
