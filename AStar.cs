﻿namespace astar_csharp
{
    public class AStar
    {
        private static int GetDistance(Point a, Point b)
        {
            int dx = Math.Abs(a.X - b.X);
            int dy = Math.Abs(a.Y - b.Y);
            return dx + dy;
        }

        public static List<Point> FindPath(Point start, Point end, List<Point> obstacles)
        {
            List<Node> openList = [];
            List<Node> closedList = [];

            Node startNode = new(start, true);
            Node endNode = new(end, true);

            List<Node> obstacleNodes = [];
            foreach (Point obstacle in obstacles)
            {
                obstacleNodes.Add(new Node(obstacle, false));
            }

            openList.Add(startNode);

            while (openList.Count > 0)
            {
                Node currentNode = openList[0];
                for (int i = 1; i < openList.Count; i++)
                {
                    if (openList[i].F < currentNode.F)
                    {
                        currentNode = openList[i];
                    }
                }

                openList.Remove(currentNode);
                closedList.Add(currentNode);

                if (currentNode.Position.Equals(endNode.Position))
                {
                    List<Point> path = new List<Point>();
                    while (currentNode != null)
                    {
                        path.Add(currentNode.Position);
                        currentNode = currentNode.Parent;
                    }
                    path.Reverse();
                    return path;
                }

                List<Point> neighbors =
                [
                    new(currentNode.Position.X - 1, currentNode.Position.Y - 1),
                    new(currentNode.Position.X - 1, currentNode.Position.Y),
                    new(currentNode.Position.X - 1, currentNode.Position.Y + 1),
                    new(currentNode.Position.X, currentNode.Position.Y - 1),
                    new(currentNode.Position.X, currentNode.Position.Y + 1),
                    new(currentNode.Position.X + 1, currentNode.Position.Y - 1),
                    new(currentNode.Position.X + 1, currentNode.Position.Y),
                    new(currentNode.Position.X + 1, currentNode.Position.Y + 1)
                ];

                foreach (Point neighborPos in neighbors)
                {
                    bool isObstacle = false;
                    foreach (Node obstacle in obstacleNodes)
                    {
                        if (obstacle.Position.Equals(neighborPos))
                        {
                            isObstacle = true;
                            break;
                        }
                    }

                    if (isObstacle)
                        continue;

                    Node neighbor = new Node(neighborPos, true, currentNode);
                    if (closedList.Contains(neighbor))
                        continue;

                    neighbor.G = currentNode.G + 1;
                    neighbor.H = GetDistance(neighbor.Position, endNode.Position);

                    if (openList.Contains(neighbor))
                    {
                        Node existingNode = openList.Find(n => n.Equals(neighbor));
                        if (neighbor.G >= existingNode.G)
                            continue;
                    }

                    openList.Add(neighbor);
                }
            }

            return [];
        }
    }

}
