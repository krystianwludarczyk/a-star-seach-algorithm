﻿using astar_csharp;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("ENTER STARTING POINT COORDINATES");
        Console.WriteLine("X:");
        int startX = int.Parse(Console.ReadLine());
        Console.WriteLine("Y:");
        int startY = int.Parse(Console.ReadLine());

        Console.WriteLine("ENTER END POINT COORDINATES");
        Console.WriteLine("X:");
        int endX = int.Parse(Console.ReadLine());
        Console.WriteLine("Y:");
        int endY = int.Parse(Console.ReadLine());

        Console.WriteLine("ENTER NUMBER OF OBSTACLES:");
        int obstacleCount = int.Parse(Console.ReadLine());
        List<Point> obstacles = new List<Point>();
        for (int i = 0; i < obstacleCount; i++)
        {
            Console.WriteLine($"ENTER OBSTACLE {i + 1} COORDINATES");
            Console.WriteLine("X:");
            int obstacleX = int.Parse(Console.ReadLine());
            Console.WriteLine("Y:");
            int obstacleY = int.Parse(Console.ReadLine());
            obstacles.Add(new Point(obstacleX, obstacleY));
        }

        Console.Clear();
        Console.WriteLine("Calculating path, please wait...");

        System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        stopwatch.Start();

        List<Point> path = AStar.FindPath(new Point(startX, startY), new Point(endX, endY), obstacles);

        stopwatch.Stop();
        TimeSpan timeElapsed = stopwatch.Elapsed;

        Console.Clear();
        Console.WriteLine("INFORMATION");
        Console.WriteLine($"Starting point > [{startX}, {startY}]");
        Console.WriteLine($"Ending point > [{endX}, {endY}]");
        Console.WriteLine($"Time taken > {timeElapsed.TotalSeconds:F4} seconds");
        Console.WriteLine($"Path length > {path.Count}");
        Console.WriteLine("Press 'T' key to display the path...");

        ConsoleKeyInfo keyInfo = Console.ReadKey();
        if (keyInfo.Key == ConsoleKey.T)
        {
            Console.WriteLine("\nPath:");
            foreach (Point point in path)
            {
                Console.WriteLine($"[{point.X},{point.Y}]");
            }
        }
    }
}
